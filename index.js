
// -------------------------------------Partie social-media---------------------------------------------

const btn = document.querySelectorAll('.image-sm');
const btn_plus = document.querySelector('.click-sm');
const btn_media = document.querySelector('.plus');

btn_plus.addEventListener('click', () =>{
    btn.forEach(b =>{
        if(!b.classList.contains('active')){
            b.classList.toggle('active');
            btn_plus.classList.add('active');
            btn_media.classList.add('active');
        }else{
            b.classList.toggle('disactive');
            btn_plus.classList.remove('active');
            btn_media.classList.remove('active');
         }
         setTimeout(() => {
             b.classList.add('disactive');
             btn_plus.classList.remove('active');
             btn_media.classList.remove('active');
         }, 8000)
    });
});

// ---------------------------------------------Flou---------------------------------------------------

const boxes = Array.from(document.querySelectorAll(".btn_titre"));
let currentIndex = 0;

boxes.forEach(box =>{
    box.addEventListener('mouseenter', e =>{
        const el = e.target;
        currentIndex = boxes.indexOf(el);

        boxes.forEach((box, index) => {
            if(index === currentIndex) return;
            box.classList.add('flou');
        });
    });
    box.addEventListener('mouseleave', () =>{
        boxes.forEach((box, index) =>{
            if(index === currentIndex) return;
            box.classList.remove('flou');
        });
    });
});
      
        
// ----------------------------------Accordion partie professionnelle------------------------------------------
        
const titres = document.querySelectorAll('.btn_titre');
const body = document.querySelectorAll('.body');

const blur = document.querySelectorAll('.blur');

titres.forEach((btn) =>{
    btn.addEventListener('click', (e) =>{

        body.forEach((bdy) =>{
            if(e.target.nextElementSibling === bdy){
                blur.forEach((b) =>{
                    b.classList.toggle('blur-window');
                });
            }
        });

        body.forEach((bd) =>{
            if(e.target.nextElementSibling !== bd && bd.classList.contains('active')) {
                bd.classList.remove('active');
                titres.forEach((btn) =>{btn.classList.remove('active');});
            }
        });
        const bodys = btn.nextElementSibling;
        bodys.classList.toggle('active');
        btn.classList.toggle('active');
    });
});

window.addEventListener('click', (e) => {
    if(!e.target.matches('.btn_titre')){
        titres.forEach((btn) => {btn.classList.remove('active')});
        body.forEach((bd) => {bd.classList.remove('active')});
        blur.forEach((b) =>{
            b.classList.remove('blur-window');
        });
    }
});

// ---------------------------------------------Accordion partie 3---------------------------------------------------

const btn_principale = document.querySelectorAll('.btn-principale');
const body_principale = document.querySelectorAll('.body-principale');

const btn_secondaire = document.querySelectorAll('.btn-secondaire');
const body_secondaire = document.querySelectorAll('body-secondaire');


btn_principale.forEach((btn) =>{
    btn.addEventListener('click', (e) =>{

        body_principale.forEach((bp) =>{
            if(e.target.nextElementSibling !== bp && bp.classList.contains('active')){
                bp.classList.remove('active');
                btn_principale.forEach((btn) => btn.classList.remove('active'));
            }
        });

        const element_principale = btn.nextElementSibling;
        element_principale.classList.toggle('active');
        btn.classList.toggle('active');
    });
});


btn_secondaire.forEach((btn) =>{
    btn.addEventListener('click', (e) =>{
        
        body_secondaire.forEach((bs) =>{
            if(e.target.nextElementSibling !== bs && bs.classList.contains('active')){
                bs.classList.remove('active');
                btn_principale.forEach((btn) => btn.classList.remove('active'));
            }
        });
        
        const element_secondaire = btn.nextElementSibling;
        element_secondaire.classList.toggle('active');
        btn.classList.toggle('active');
    });
});

window.addEventListener('click', (e) => {
    let result = true;
    if(!e.target.matches('.btn-principale')){
            if(!e.target.matches('.btn-secondaire')){
                btn_secondaire.forEach((btn) => btn.classList.remove('active'));
                body_secondaire.forEach((bp) => bp.classList.remove('active'));
                result = false;
            }
            if(result == false){
                btn_principale.forEach((btn) => btn.classList.remove('active'));
                body_principale.forEach((bp) => bp.classList.remove('active'));
        }
    }
});

// ---------------------------------------------partie scroll--------------------------------------------------

const scroll = document.querySelector('.up-nav');

scroll.addEventListener('click', ()=> {
    setTimeout(() =>{
        window.scrollTo({
            top,
            behavior: 'smooth'
        });
    }, 0);
});

window.addEventListener('scroll', ()=>{
    if (window.scrollY > 100) {
        scroll.classList.add('active');
    }else{
        scroll.classList.remove('active');
    }
});